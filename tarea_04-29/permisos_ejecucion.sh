#!/bin/bash

#Tomar el nombre del archivo a buscar
read -p "Por favor ingrese el nombre del archivo: " archivo

#Verifico si el archivo existe, de lo contrario se sale
if
         test -e $archivo ;then
        echo "El archivo existe"
else
         echo "El archivo no existe"
        exit
fi

#si el archivo existe verifico permisos de ejecución
#luego pregunto si desea ejecutarlo
#en ambos casos no lo ejecuta

if
        test -x $archivo ; then
        ls -l $archivo
	read -p "¿Querés ejecutarlo?(si/no) " ejecucion

		if
        		test $ejecucion = "si"; then
        		echo "Ejecutando fichero"
			sleep 5

		else
			test $ejecucion = "no"
			echo "El fichero no será ejecutado"
        		sleep 5
		fi
fi
#Verifico si el archivo no tiene permiso de ejecución
#pero si es propietario
#si es propietario y no tiene permiso de ejecución
#preguntar si quiere otorgar dicho permiso
#si la respuesta es positiva otorga permiso y lista detalle del archivo
if
	!(test -x $archivo) && test -O $archivo; then
	read -p "¿Querés otorgar permisos de ejecución?(s/n): " s n
	if
		test $s = "s" ; then
		chmod +x $archivo && ls -l $archivo
	else
		echo "Adios"
		exit
	fi
else
	echo "Adios"
fi
