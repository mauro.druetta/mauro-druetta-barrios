#!/bin/bash
set -x

echo "Uso del comando: 
ping.sh <-C> (cantidad pings) <-T> (timestamp) <-p> (ipv4 o ipv6) <-b> (Permitir ping a broadcast)"
sleep 5

args=( $@ )

for opcion in "${!args[@]}"; do
	case ${args[$opcion]} in
		-C) cantidad=${args[$(($opcion+1))]}
		    if [ $cantidad -gt 0 ]; then
		    	counter="-c $cantidad"
		    else
		     	echo "Para opcion -C debe ser un número entero mayor a cero"
			exit
		    fi
		    ;;
		-T) timestamp="-D"
		;;
		-p) proto=${args[$(($opcion+1))]}
	   	    if [[ $proto =~ [46] ]]; then
		    	p="-$proto"
		    else
			echo "Para opcion -p use 4 0 6"
			exit
		    fi
		;;
		-b) b="-b"
		;;
		#*)
		   #if  [[ !${args[$opcion]} == ]]; then 
		   #echo "Estas ingresando argumentos invalidos o inexistentes"
		   #exit 
		#;;
	esac
done
if [ ${#args[opcion]} -lt 1 ]; then
        echo "Debe ingresar al menos una opcion"
        exit
fi


evalhostname () {
	local ip="$1"
	local stat=1
	local IFS="." read ip1 ip2 ip3 ip4<<< "$ip"
	if [[ $ip =~ ^[0-9]{1,4}\.[0-9]{1,4}\.[0-9]{1,4}\.[0-9]{1,4}$ ]]; then
		[[ ${ip[1]} -le 255 && ${ip[2]} -le 255 && ${ip[3]} -le 255 && ${ip[4]} -le 255 ]]
                stat=$?
        fi
        return $stat
}

read -p "Ingrese hostname o ip: " hostname

if     [[ "$hostname" =~ [0-9] ]] ; then
	evalhostname "$hostname"
		if [[ "$?" -ne 0 ]];then
  			echo "IP invalida ($hostname)"
			exit
		fi
fi

if  [ -z $hostname ]; then
    echo "Debe ingresar hostname o ip valido al final"
    exit
 
fi

ping $b $timestamp $p $counter $hostname
