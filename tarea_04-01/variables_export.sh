#! /bin/bash
#Primero creo y  exporto las variables para que estén disponibles en otro script
export VAR1="Consultaste las variables el: "
export VAR2="$(date)"

#Luego consulto las variables y las imprimo en pantalla
echo "$VAR1 $VAR2"

#EOF
